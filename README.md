# VirtualClassroomFunctions

- getClassroomsLive()
- getClassroomsUpcoming()
- getClassroomsPast()
- getChatMessages(string classroomId)
- postChatMessage(string classroomId, string message)
- subscribeClassroom(string classroomId)
- updateClassroomSpeech(string classroomId)
- postAttachment(string classroomId, byte[] data ??)
- downloadAttachment(string attachmentId)
